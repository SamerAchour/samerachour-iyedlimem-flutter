import 'package:flutter/material.dart';

class BoardCard extends StatelessWidget {
  final String _image;
  final String _name;
  final String _job;
  final String _budget;
  const BoardCard( this._image, this._name, this._job, this._budget,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 321,
      height: 76,
      margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            color: Color(0x0c000000),
            blurRadius: 25,
            offset: Offset(0, 10),
          ),
        ],
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 55,
            height: 55,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("https://flenci.herokuapp.com/"+_image),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.circular(12),
            ),

          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [

                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                    child: Text(
                      _name,
                      style: TextStyle(
                        color: Color(0xff363636),
                        fontSize: 11,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),

              Column(
                children: [
                  Text(
                    _job,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 9,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    _budget.toString()+"TND",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 9,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              )


            ],
          ),
          Container(
            width: 30,
            height: 30,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(11),
              color: Color(0xfffbd9ea),
            ),
            child: Icon(Icons.visibility,color: Color(0xffc7357c),),
          )
        ],
      ),
    );
  }
}
