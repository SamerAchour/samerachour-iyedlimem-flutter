import 'package:flutter/material.dart';

class ProfileValue extends StatelessWidget {
  final String _value;
  final String _hint;
  const ProfileValue(this._value,this._hint,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 115,
      height: 115,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color(0x0c000000),
            blurRadius: 25,
            offset: Offset(0, 10),
          ),
        ],
        color: Color(0xffe187b3),
      ),
     child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            _value,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
              fontFamily: "Inter",
              fontWeight: FontWeight.w600,
            ),
          ),
          Text(
            _hint,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: "Inter",
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      )



    );

  }
}
