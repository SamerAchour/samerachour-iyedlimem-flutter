import 'package:Flenci/models/project_class.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ProjectInProgress extends StatefulWidget {

  final String _title;
  final String _duration;
  final String _instaid;
  final String _user;


  const ProjectInProgress(this._title,this._duration,this._instaid,this._user,{Key? key}) : super(key: key);

  @override
  State<ProjectInProgress> createState() => _ProjectInProgressState();
}

class _ProjectInProgressState extends State<ProjectInProgress> {

  late Project project;
  final String _baseUrl = "flenci.herokuapp.com";
  late String? insta;
  late Future<bool> fetchedProject ;
  Future<bool> getProject()async{
    http.Response response = await http.get(Uri.http(_baseUrl, "/api/insta/"+widget._instaid));

    dynamic requestServer = json.decode(response.body);
     print(requestServer);
insta = requestServer["profile_picture_url"];
    return true;
  }
  @override
  void initState() {
   fetchedProject= getProject();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchedProject,
        builder: (BuildContext context,AsyncSnapshot<bool> snapshot){
        if(snapshot.hasData){
          return Container(
            width: 308,
            height: 68,
            margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Color(0x0c000000),
                  blurRadius: 25,
                  offset: Offset(0, 10),
                ),
              ],
              color: Colors.white,
            ),

            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage("https://flenci.herokuapp.com/"+widget._user),
                      fit: BoxFit.cover
                    ),
                    shape: BoxShape.circle,
                  ),

                ),
                Icon(Icons.sync_alt),
                Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:   NetworkImage(insta!),
                    ),
                    shape: BoxShape.circle,
                  ),

                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                     widget._title,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                     widget._duration,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),

                Container(
                  width: 30,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(11),
                    color: Color(0xfffbd9ea),
                  ),
                  child: Icon(Icons.visibility,color: Color(0xffc7357c),),
                )

              ],
            ),
          );
        }else{
          return const Center(
            child: const CircularProgressIndicator(color:  Color(0xffc7357c),),
          );
        }
        });

  }
}
