import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotifCard extends StatelessWidget {
  final String _title;
  final String _time;
  final String _contenu;
  const NotifCard(this._title, this._time, this._contenu, {Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
        width: 321,
        height: 45,
        margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: 45,
                  height: 45,
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/influ.jpg")),
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    _title,
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w600,
                    ),
                  ),

                  Container(
                    width: 230,

                    child:Row(
                      children: [
                        Expanded(
                          child: Text(
                            _contenu,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: TextStyle(

                              color: Color(0xff636363),
                              fontSize: 11,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    )
                  ),
                ]),
              ],
            ),
            Text(
              _time,
              style: TextStyle(
                color: Color(0xff636363),
                fontSize: 11,
                fontFamily: "Inter",
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
        Divider(
          height: 2,
          color: Colors.grey,
        )
    ]);
  }
}
