import 'package:Flenci/screens/menu.dart';
import 'package:Flenci/screens/messagerie.dart';
import 'package:Flenci/screens/notif_menu.dart';
import 'package:Flenci/screens/profilecompanyowner.dart';
import 'package:flutter/material.dart';



class NavBotOwner extends StatefulWidget {
  const NavBotOwner({Key? key}) : super(key: key);

  @override
  _NavBotOwnerState createState() => _NavBotOwnerState();
}

class _NavBotOwnerState extends State<NavBotOwner> {

  int _currentIndex = 1;
  final List<Widget> _interfaces = [
    const NotificationScreen(),
    const Menu(),
    const Messagerie(),


  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _interfaces[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Color(0xffcb337d),
        items: const [
          BottomNavigationBarItem(label: "Notification", icon: Icon(Icons.notifications)),
          BottomNavigationBarItem(
            label: "Menu", icon:Icon(
            Icons.home,
          ),),
          BottomNavigationBarItem(label: "Chat", icon: Icon(Icons.message)),

        ],
        currentIndex: _currentIndex,
        onTap: (int value) {
          setState(() {
            _currentIndex = value;
          });
        },
      ),
    );
  }
}
