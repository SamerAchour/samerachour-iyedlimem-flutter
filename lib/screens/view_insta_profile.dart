import 'dart:ui';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:Flenci/widgets/profile_value.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

class ViewInstaProfile extends StatefulWidget {
  const ViewInstaProfile({Key? key}) : super(key: key);

  @override
  _ViewInstaProfileState createState() => _ViewInstaProfileState();
}

class _ViewInstaProfileState extends State<ViewInstaProfile> {
  bool _reach = true;
  bool _pub = true;
  bool _impression = true;
  bool _views = true;
  late String? _userId;
  late String? _influId;
  late String? _influimg;
  late String? _influname;
  late String? _influbio;
  late String? _influcomment;
  late String? _influengagment;
  late String? _influimpresseon;
  late String? _influfollowers;
  late String? _influposts;
  late String? _influusername;
  final String _baseUrl = "flenci.herokuapp.com";
  final GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  late String? _otitle;
  late String? _obudget;
  late String? _duration;
  late String? _description;

  late Future<bool> profileData;
  Future<bool> getProfileData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _influimg = prefs.getString("influimg");
    _userId=prefs.getString("userId");
    _influId=prefs.getString("influid");
    _influname = prefs.getString("influname");
    _influbio = prefs.getString("influbio");
    _influcomment = prefs.getString("influcomment");
    _influengagment = prefs.getString("influengagment");
    _influimpresseon = prefs.getString("influimpresseon");
    _influusername = prefs.getString("influusername");
    _influfollowers = prefs.getInt("influfollowers").toString();
    _influposts = prefs.getInt("influposts").toString();
    return true;
  }

  @override
  void initState() {
    profileData = getProfileData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 50),
            ListTile(
              title: Row(
                children: [
                  Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/influ.jpg")),
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Color(0xffe52b5d),
                        width: 2.50,
                      ),
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(width: 10),
                  const Text(
                    'Samer Achour',
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, "/");
              },
            ),
            ListTile(
                title: Divider(
              height: 000,
              color: Colors.black,
              thickness: 2,
            )),
            ListTile(
              title: Row(
                children: [
                  const Text('Work In Progress'),
                  SizedBox(
                    width: 6,
                  ),
                  Icon(Icons.event_note_sharp),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, "/owner/profile");
              },
            ),
            ListTile(
              title: Row(
                children: [
                  const Text('Sign Out'),
                  SizedBox(
                    width: 6,
                  ),
                  Icon(Icons.logout),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, "/");
              },
            ),
            ListTile(
              title: Row(
                children: [
                  const Text('Edit Profil'),
                  SizedBox(
                    width: 6,
                  ),
                  Icon(Icons.edit),
                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, "/owner/edit");
              },
            ),
            ListTile(
              title: Row(
                children: [
                  const Text('Delete Account'),
                  SizedBox(
                    width: 6,
                  ),
                  Icon(Icons.delete),
                ],
              ),
              onTap: () {},
            ),
          ],
        )),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
            ),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Builder(builder: (context) {
                return InkWell(
                  child: Container(
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/influ.jpg")),
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Color(0xffe52b5d),
                        width: 2.50,
                      ),
                      color: Colors.white,
                    ),
                  ),
                  onTap: () => Scaffold.of(context).openDrawer(),
                );
              }),
              Container(
                width: 123,
                height: 30,
                child: Image.asset("assets/images/logoFlenci.png"),
              ),
              Expanded(
                child: const Text(
                  "Profil",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Mada",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: FutureBuilder(
          future: profileData,
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              return ListView(children: [
                Container(
                  width: 110,
                  height: 110,
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Color(0xffe52b5d),
                      width: 2.50,
                    ),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Container(
                      width: 96,
                      height: 92,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(_influimg!), fit: BoxFit.cover),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    _influname!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff363636),
                      fontSize: 22,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Container(
                  width: 167,
                  height: 40,
                  child: Text(
                    _influbio!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  child: Container(
                      width: 89,
                      height: 35,
                      child: Text(_influfollowers! + "\nFollowers",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ))),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(60, 20, 60, 20),
                  child: Divider(
                    height: 2,
                    color: Color(0xff5e5e5e),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkWell(
                            child: _reach
                                ? Container(
                                    width: 115,
                                    height: 115,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x0c000000),
                                          blurRadius: 25,
                                          offset: Offset(0, 10),
                                        ),
                                      ],
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.bar_chart,
                                          color: Color(0xffe187b3),
                                          size: 75,
                                        ),
                                        Text(
                                          "Engagement",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ))
                                : ProfileValue(_influengagment!, "Engagment"),
                            onTap: () async {
                              setState(() {
                                _reach = !_reach;
                              });
                            },
                          ),
                          InkWell(
                            child: _pub
                                ? Container(
                                    width: 115,
                                    height: 115,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x0c000000),
                                          blurRadius: 25,
                                          offset: Offset(0, 10),
                                        ),
                                      ],
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.backup_table,
                                          color: Color(0xffe187b3),
                                          size: 75,
                                        ),
                                        Text(
                                          "Publications",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : ProfileValue(_influposts!, "Publications"),
                            onTap: () async {
                              setState(() {
                                _pub = !_pub;
                              });
                            },
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          InkWell(
                            child: _impression
                                ? Container(
                                    width: 115,
                                    height: 115,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x0c000000),
                                          blurRadius: 25,
                                          offset: Offset(0, 10),
                                        ),
                                      ],
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.favorite_border,
                                          color: Color(0xffe187b3),
                                          size: 75,
                                        ),
                                        Text(
                                          "Impressions",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ))
                                : ProfileValue(
                                    _influimpresseon!, "Impressions"),
                            onTap: () async {
                              setState(() {
                                _impression = !_impression;
                              });
                            },
                          ),
                          InkWell(
                            child: _views
                                ? Container(
                                    width: 115,
                                    height: 115,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0x0c000000),
                                          blurRadius: 25,
                                          offset: Offset(0, 10),
                                        ),
                                      ],
                                      color: Colors.white,
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.forum,
                                          color: Color(0xffe187b3),
                                          size: 75,
                                        ),
                                        Text(
                                          "Comments",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ))
                                : ProfileValue(_influcomment!, "Comments"),
                            onTap: () async {
                              setState(() {
                                _views = !_views;
                              });
                            },
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              width: 141,
                              height: 29,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(22),
                                color: Colors.white,
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                          side: BorderSide(
                                              color: Color(0xffbd3784))),
                                    ),
                                    elevation: MaterialStateProperty.all(0.0),
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.white)),
                                onPressed: () async {
                                  await launch("https://instagram.com/" +
                                      _influusername!);
                                },
                                child: Text(
                                  "instagram",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color(0xffbd3784),
                                    fontSize: 13,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              width: 139,
                              height: 29,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(22),
                                gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    Color(0xffeb2834),
                                    Color(0xffca337e),
                                    Color(0xffa93d8c)
                                  ],
                                ),
                              ),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                    ),
                                    elevation: MaterialStateProperty.all(0.0),
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.transparent)),
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return BackdropFilter(
                                          child: AlertDialog(
                                            title: const Text(
                                              "Collaboration request",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0xff363636),
                                                fontSize: 18,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 0.72,
                                              ),
                                            ),
                                            content: Container(
                                              height: 464,
                                              width: 321,
                                              child: Center(
                                                child: Form(
                                                  key: _keyForm,
                                                  child: ListView(
                                                    children: [
                                                      Container(
                                                        width: 248,
                                                        height: 50,
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            0, 20, 0, 0),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(20),
                                                          color:
                                                              Color(0xffe6e3e3),
                                                        ),
                                                        child: TextFormField(
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .black),
                                                          decoration:
                                                              const InputDecoration(
                                                            border: InputBorder
                                                                .none,
                                                            hintText:
                                                                "Offre Title",
                                                            hintStyle:
                                                                TextStyle(
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    7.0),
                                                          ),
                                                          onSaved:
                                                              (String? value) {
                                                            _otitle = value;
                                                          },
                                                        ),
                                                      ),
                                                      Container(
                                                        width: 248,
                                                        height: 50,
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            0, 20, 0, 0),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(20),
                                                          color:
                                                              Color(0xffe6e3e3),
                                                        ),
                                                        child: TextFormField(
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .black),
                                                          decoration:
                                                              const InputDecoration(
                                                            border: InputBorder
                                                                .none,
                                                            hintText:
                                                                "Estimated Budget",
                                                            hintStyle:
                                                                TextStyle(
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    7.0),
                                                          ),
                                                          onSaved:
                                                              (String? value) {
                                                            _obudget = value;
                                                          },
                                                        ),
                                                      ),
                                                      Container(
                                                        width: 248,
                                                        height: 50,
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            0, 20, 0, 0),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(20),
                                                          color:
                                                              Color(0xffe6e3e3),
                                                        ),
                                                        child: TextFormField(
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .black),
                                                          decoration:
                                                              const InputDecoration(
                                                            border: InputBorder
                                                                .none,
                                                            hintText:
                                                                "Duration",
                                                            hintStyle:
                                                                TextStyle(
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    7.0),
                                                          ),
                                                          onSaved:
                                                              (String? value) {
                                                            _duration = value;
                                                          },
                                                        ),
                                                      ),
                                                      Container(
                                                        width: 248,
                                                        height: 116,
                                                        margin: const EdgeInsets
                                                                .fromLTRB(
                                                            0, 20, 0, 0),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(20),
                                                          color:
                                                              Color(0xffe6e3e3),
                                                        ),
                                                        child: TextFormField(
                                                          keyboardType:
                                                              TextInputType
                                                                  .multiline,
                                                          maxLines: null,
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .black),
                                                          decoration:
                                                              const InputDecoration(
                                                            border: InputBorder
                                                                .none,
                                                            hintText:
                                                                " Offre Description",
                                                            hintStyle:
                                                                TextStyle(
                                                              color:
                                                                  Colors.black,
                                                            ),
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    7.0),
                                                          ),
                                                          onSaved:
                                                              (String? value) {
                                                            _description =
                                                                value;
                                                          },
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 20,
                                                      ),
                                                      Center(
                                                        child: Container(
                                                          margin:
                                                              const EdgeInsets
                                                                      .fromLTRB(
                                                                  0, 20, 0, 0),
                                                          height: 50,
                                                          child: ElevatedButton(
                                                            onPressed: () {
                                                              if (_keyForm
                                                                  .currentState!
                                                                  .validate()) {
                                                                _keyForm
                                                                    .currentState!
                                                                    .save();
                                                                Map<String, dynamic> userData = {
                                                                  "title": _otitle,
                                                                  "budget": _obudget,
                                                                  "duration": _duration,
                                                                  "description": _description,
                                                                };
                                                                Map<String, String> headers = {
                                                                  "Content-Type": "application/json; charset=UTF-8"
                                                                };
                                                                http
                                                                    .post(Uri.https(_baseUrl, "/api/request/add/"+_userId!+"/"+_influId!),
                                                                    headers: headers, body: json.encode(userData))
                                                                    .then((http.Response response) async{
                                                                          if(response.statusCode == 200){
                                                                            showDialog(
                                                                                context: context,
                                                                                builder: (BuildContext context) {
                                                                                  return const AlertDialog(
                                                                                    title: Text("Information"),
                                                                                    content: Text("request sent "),
                                                                                  );
                                                                                });
                                                                          }else if (response.statusCode == 404){
                                                                            print(response.body);
                                                                          }else{
                                                                           print(response.body);
                                                                          }
                                                                });
                                                                }
                                                            },
                                                            style: ButtonStyle(
                                                                shape: MaterialStateProperty.all<
                                                                        RoundedRectangleBorder>(
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              0.0),
                                                                )),
                                                                elevation:
                                                                    MaterialStateProperty
                                                                        .all(
                                                                            0.0),
                                                                backgroundColor:
                                                                    MaterialStateProperty
                                                                        .all(Colors
                                                                            .white)),
                                                            child: Ink(
                                                              decoration:
                                                                  BoxDecoration(
                                                                      gradient:
                                                                          const LinearGradient(
                                                                        colors: [
                                                                          Color(
                                                                              0xffEB2834),
                                                                          Color(
                                                                              0xffCA337E),
                                                                          Color(
                                                                              0xffA93D8C)
                                                                        ],
                                                                        begin: Alignment
                                                                            .centerLeft,
                                                                        end: Alignment
                                                                            .centerRight,
                                                                      ),
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              30.0)),
                                                              child: Container(
// ink width to fit-parent
                                                                constraints: const BoxConstraints(
                                                                    minWidth: double
                                                                        .infinity,
                                                                    minHeight:
                                                                        30.0),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child:
                                                                    const Text(
                                                                  "Send Request",
                                                                  textAlign:
                                                                      TextAlign
                                                                          .center,
                                                                  style: TextStyle(
                                                                      fontFamily:
                                                                          'Mada',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      fontSize:
                                                                          18,
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                            elevation: 10,
                                            shape: const RoundedRectangleBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(32.0))),
                                          ),
                                          filter: ImageFilter.blur(
                                              sigmaX: 6, sigmaY: 6),
                                        );
                                      });
                                },
                                child: Text(
                                  "Send Request",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ]);
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
