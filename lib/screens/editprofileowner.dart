import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  String? dropdownValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(children: [
        Container(
          child: Container(
              height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x0c000000),
                    blurRadius: 15,
                    offset: Offset(0, 10),
                  ),
                ],
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: IconButton(
                      icon: Icon(Icons.chevron_left_outlined),
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, "/owner/profile");
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                    child: const Text(
                      "Edit Profile",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: "Mada",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  )
                ],
              )),
        ),
        Container(
          width: 110,
          height: 110,
          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Color(0xffe52b5d), width: 2.50, ),
            color: Colors.white,
          ),
          child: Center(
            child: Container(
              width: 96,
              height: 92,

              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: Image.asset("assets/images/Ellipse.png"),
            ),
          ),

        ),
        Container(
          margin: const EdgeInsets.fromLTRB(40, 15, 40, 20),
          width: 308,
          height: 320,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            boxShadow: const [
              BoxShadow(
                color: Color(0x0c000000),
                blurRadius: 25,
                offset: Offset(0, 10),
              ),
            ],
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(40, 20, 40, 10),
                child: TextFormField(
                  decoration: const InputDecoration(
                      suffixIcon: Icon(
                        Icons.account_circle,
                        color: Colors.black,
                      ),
                      border: UnderlineInputBorder(),
                      labelText: "Company name"),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
                width:306,
                height: 64,
                child: DropdownButton<String>(
                  value: dropdownValue,
                  hint: Text('Field of business'),
                  icon: const Icon(Icons.arrow_drop_down),
                  isExpanded: true,
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValue = newValue!;
                    });

                  },
                  items: <String>['Entertainment', 'Food', 'health and Style']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
                child: TextFormField(
                  decoration: const InputDecoration(
                      suffixIcon: Icon(
                        Icons.mail,
                        color: Colors.black,
                      ),
                      border: UnderlineInputBorder(),
                      labelText: "Email"),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(40, 0, 40, 10),
                child: TextFormField(
                  obscureText: true,
                  decoration: const InputDecoration(
                      suffixIcon: Icon(
                        Icons.visibility_off_outlined,
                        color: Colors.black,
                      ),
                      border: UnderlineInputBorder(),
                      labelText: "New Password"),
                ),
              ),
            ],
          ),
        ),
        const Center(
          child: Text(
            "write your password to confirm new changes",
            style: TextStyle(
              color: Color(0xff363636),
              fontSize: 12,
              fontFamily: "Inter",
              fontWeight: FontWeight.w600,
              letterSpacing: 0.40,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(40, 15, 40, 20),
          width: 308,
          height: 70,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            boxShadow: const [
              BoxShadow(
                color: Color(0x0c000000),
                blurRadius: 25,
                offset: Offset(0, 10),
              ),
            ],
            color: Colors.white,
          ),
          child: Container(
            margin: const EdgeInsets.fromLTRB(40, 0, 40, 0),
            child: TextFormField(
              obscureText: true,
              decoration: const InputDecoration(
                  suffixIcon: Icon(
                    Icons.visibility_off_outlined,
                    color: Colors.black,
                  ),
                  border: UnderlineInputBorder(),
                  labelText: "Password"),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(50, 15, 50, 0),
          height: 50.0,
          width: 100.0,
          //width: double.infinity,

          child: ElevatedButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, "/owner/profile");
            },
            style: ButtonStyle(
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0.0),
                )),
                elevation: MaterialStateProperty.all(0.0),
                backgroundColor: MaterialStateProperty.all(Colors.white)),
            child: Ink(
              decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    colors: [
                      Color(0xffEB2834),
                      Color(0xffCA337E),
                      Color(0xffA93D8C)
                    ],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                // ink width to fit-parent
                constraints: const BoxConstraints(
                    minWidth: double.infinity, minHeight: 50.0),
                alignment: Alignment.center,
                child: const Text(
                  "Confirm",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Mada',
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.white),
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
