import 'dart:ui';

import 'package:flutter/material.dart';


class Signup extends StatelessWidget {
  const Signup({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register",style: TextStyle(
            fontSize: 20, color: Colors.black87, fontFamily: 'Mada')),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),

      body: Form(
        child: ListView(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(0 ,20, 20, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Container(
                      width: 40,
                      height: 15,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(22),
                        gradient: const LinearGradient(begin: Alignment.centerLeft, end: Alignment.centerRight, colors: [Color(0xffeb2834), Color(0xffca337e), Color(0xffa93d8c)], ),
                      ),
                    ),
                    const SizedBox(width: 10,),
                    Container(
                      width: 15,
                      height: 15,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xff8983d7),
                      ),
                    ),

                  ],),
              ),
              Container(
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(20, 40, 20, 200),

                  child: Image.asset("assets/images/logoFlenci.png", width: 50, height: 50)),
              Padding(padding: EdgeInsets.fromLTRB(0, 0, 0, 0) ,
                  child:
                  Column (
                    children: [
                      Container(
                        margin: const EdgeInsets.fromLTRB(20, 40, 20, 20),
                        child:
                        ConstrainedBox(
                          constraints: const BoxConstraints.tightFor(width: 350, height: 53),
                          child:ElevatedButton(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.asset("assets/images/flenciIcon.png", width: 50, height: 50),
                                const Text('Register as proejct owner',  textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Mada',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.black))
                              ],
                            ),
                            style: ButtonStyle(
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                      side: BorderSide(color: Colors.black)
                                  ),),


                                elevation: MaterialStateProperty.all(0.0),
                                backgroundColor: MaterialStateProperty.all(Colors.white)),

                            onPressed: () {
                              Navigator.pushReplacementNamed(context, "/signupflenci");
                            },
                          ),
                        ),
                      ),


                      Container(
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return BackdropFilter(
                                    child: AlertDialog(
                                      title: const Text(
                                        "Sign Form",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Color(0xff363636),
                                          fontSize: 18,
                                          fontFamily: "Inter",
                                          fontWeight: FontWeight.w600,
                                          letterSpacing: 0.72,
                                        ),
                                      ),
                                      content: Container(
                                        height: 300,
                                        width: 321,
                                        child: Center(
                                          child: ListView(
                                            children: [
                                              Container(
                                                width: 248,
                                                height: 50,
                                                margin:
                                                const EdgeInsets.fromLTRB(
                                                    0, 20, 0, 0),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(20),
                                                  color: Color(0xffe6e3e3),
                                                ),
                                                child: TextFormField(

                                                  style: const TextStyle(
                                                      color: Colors.black),
                                                  decoration:
                                                  const InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText: "Instagram user name..",
                                                    hintStyle: TextStyle(
                                                      color: Colors.black,
                                                    ),
                                                    contentPadding:
                                                    EdgeInsets.all(7.0),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                width: 248,
                                                height: 50,
                                                margin:
                                                const EdgeInsets.fromLTRB(
                                                    0, 20, 0, 0),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(20),
                                                  color: Color(0xffe6e3e3),
                                                ),
                                                child: TextFormField(
                                                  obscureText: true,
                                                  style: const TextStyle(
                                                      color: Colors.black),
                                                  decoration:
                                                  const InputDecoration(
                                                    suffixIcon: Icon(Icons.remove_red_eye_sharp,color:Colors.black,),
                                                    border: InputBorder.none,
                                                    hintText:
                                                    "Password..",
                                                    hintStyle: TextStyle(
                                                      color: Colors.black,
                                                    ),
                                                    contentPadding:
                                                    EdgeInsets.all(7.0),
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                width: 248,
                                                height: 50,
                                                margin:
                                                const EdgeInsets.fromLTRB(
                                                    0, 20, 0, 0),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(20),
                                                  color: Color(0xffe6e3e3),
                                                ),
                                                child: TextFormField(
                                                  obscureText: true,
                                                  style: const TextStyle(
                                                      color: Colors.black),
                                                  decoration:
                                                  const InputDecoration(
                                                    suffixIcon: Icon(Icons.remove_red_eye_sharp,color:Colors.black,),
                                                    border: InputBorder.none,
                                                    hintText: "Confirm your password..",
                                                    hintStyle: TextStyle(
                                                      color: Colors.black,
                                                    ),
                                                    contentPadding:
                                                    EdgeInsets.all(7.0),
                                                  ),
                                                ),
                                              ),

                                              const SizedBox(
                                                height: 20,
                                              ),
                                              Center(
                                                child: Container(
                                                  margin:
                                                  const EdgeInsets.fromLTRB(0, 20, 0, 0),
                                                  width: 200, height: 40,
                                                  child: ElevatedButton(
                                                    onPressed: () {Navigator.pushReplacementNamed(context, "/insta/navbot");},
                                                    style: ButtonStyle(
                                                        shape: MaterialStateProperty.all<
                                                            RoundedRectangleBorder>(
                                                            RoundedRectangleBorder(
                                                              borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                  0.0),
                                                            )),
                                                        elevation:
                                                        MaterialStateProperty
                                                            .all(0.0),
                                                        backgroundColor:
                                                        MaterialStateProperty
                                                            .all(Colors
                                                            .white)),
                                                    child: Ink(
                                                      decoration: BoxDecoration(
                                                          gradient:
                                                          const LinearGradient(
                                                            colors: [
                                                              Color(0xffEB2834),
                                                              Color(0xffCA337E),
                                                              Color(0xffA93D8C)
                                                            ],
                                                            begin: Alignment
                                                                .centerLeft,
                                                            end: Alignment
                                                                .centerRight,
                                                          ),
                                                          borderRadius:
                                                          BorderRadius
                                                              .circular(
                                                              30.0)),
                                                      child: Container(
// ink width to fit-parent
                                                        constraints:
                                                        const BoxConstraints(
                                                            minWidth: double
                                                                .infinity,
                                                            minHeight:
                                                            30.0),
                                                        alignment:
                                                        Alignment.center,
                                                        child: const Text(
                                                          "Sign me in",
                                                          textAlign:
                                                          TextAlign.center,
                                                          style: TextStyle(
                                                              fontFamily:
                                                              'Mada',
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize: 18,
                                                              color:
                                                              Colors.white),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      elevation: 10,
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(32.0))),
                                    ),
                                    filter:
                                    ImageFilter.blur(sigmaX: 6, sigmaY: 6),
                                  );
                                });
                          },

                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(0.0),
                                  )),
                              elevation: MaterialStateProperty.all(0.0),
                              backgroundColor: MaterialStateProperty.all(Colors.white)),
                          child: Ink(
                            decoration: BoxDecoration(
                                gradient: const LinearGradient(
                                  colors: [
                                    Color(0xffEB2834),
                                    Color(0xffCA337E),
                                    Color(0xffA93D8C)
                                  ],
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                ),
                                borderRadius: BorderRadius.circular(30.0)),
                            child: Container(
                              // ink width to fit-parent
                              constraints: const BoxConstraints(
                                  minWidth: double.infinity, minHeight: 50.0),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Image.asset("assets/images/insta.png", width: 25, height: 25),
                                  const Text(
                                    "Register as an influencer",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: 'Mada',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.white),
                                  ),
                                ],
                              )
                            ),
                          ),
                        ),
                      ),

                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 100, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text("You already have an account ?"),
                            const SizedBox(
                              width: 5,
                            ),
                            GestureDetector(
                              child: const Text("Login",
                                  style: TextStyle(
                                      color: Color(0xffE42B46),
                                      fontFamily: 'Mada',
                                      fontWeight: FontWeight.bold)),
                              onTap: () {
                                Navigator.pushReplacementNamed(context, "/signin");
                                //   Navigator.pushNamed(context, "/resetPwd");,
                              },
                            )
                          ],
                        ),
                      )

                    ],
                  )

              ),


            ]
        ),

      ),

    );

  }
}
