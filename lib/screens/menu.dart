import 'package:Flenci/models/instagrameuse_class.dart';
import 'package:Flenci/widgets/menu_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Menu extends StatefulWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {

  final List<Influ> _instagrameuseList=[];
  final String _baseUrl = "flenci.herokuapp.com";
  late Future<bool> fetchedInfluencers ;
  late String? _name;
  late String? _img;
  Future<bool> getInfluencers()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _name = prefs.getString("userName");
    _img = prefs.getString("userImg");

    http.Response response = await http.get(Uri.http(_baseUrl, "/api/insta"));
    List<dynamic> influfromServer = json.decode(response.body);

    for (int i = 0; i < influfromServer.length; i++) {
      _instagrameuseList.add(Influ(
          influfromServer[i]["username"],  influfromServer[i]["_id"]));
    }
    return true;
  }

  @override
  void initState() {

fetchedInfluencers=getInfluencers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  FutureBuilder(
        future: fetchedInfluencers,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot){
          if(snapshot.hasData){
     return Scaffold(
      drawer: Container(
        width: 220,
        child: Drawer(
            child: Column(
             mainAxisAlignment: MainAxisAlignment.start,

             children: [

               ListTile(
                 tileColor:  Color(0xfffbbcdb),
                 title: Container(
                   height: 150,
                   width: 250,

                   child: Center(
                     child: Column(


                       children: [
                         SizedBox(height: 30),
                         Container(


                           width: 60,
                           height: 60,
                           decoration: BoxDecoration(
                             image: DecorationImage(
                                 fit: BoxFit.cover,
                                 image: NetworkImage("https://flenci.herokuapp.com/"+_img!)
                             ),
                             shape: BoxShape.circle,
                             border: Border.all(
                               color: Color(0xffe52b5d),
                               width: 2,
                             ),
                             color: Colors.white,
                           ),

                         ),
                         SizedBox(height: 10),
                          Text(_name!,style: TextStyle(fontSize: 20,color:Color(0xffe52b5d), fontWeight: FontWeight.bold,),),
                       ],
                     ),
                   ),
                 ),
                 onTap: () {
                   Navigator.pushReplacementNamed(context, "/owner/profile");
                 },


               ),
               SizedBox(height: 30),

               ListTile(
                 title: Row(

                   children: [

                     Icon(Icons.event_note_sharp),
                     SizedBox ( width: 30,),
                     const Text('Work In Progress'),

                   ],
                 ),
                 onTap: () {
                   Navigator.pushNamed(context, "/owner/profile");
                 },
                 hoverColor:  Color(0xfffbbcdb),
               ),
            ListTile(

              title: Row(

              children: [

                Icon(Icons.logout),
                SizedBox ( width: 30,),
                  const Text('Sign Out'),

                ],
              ),
              onTap: ()async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();
                Navigator.pushNamed(context, "/signin");
              },
            ),
            ListTile(
              title: Row(


                children: [

                  Icon(Icons.edit),
                  SizedBox ( width: 30),
                  const Text('Edit Profil'),

                ],
              ),
              onTap: () {
                Navigator.pushNamed(context, "/owner/edit");
              },
            ),
            ListTile(
              title: Row(


                children: [

                  Icon(Icons.delete),
                  SizedBox ( width: 30),
                  const Text('Delete Account'),

                ],
              ),
              onTap: () {},
            ),
          ],
        )),
      ),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[Container()],
        toolbarHeight: 100,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        title:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Builder(
                  builder: (context) {
                    return InkWell(
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage("https://flenci.herokuapp.com/"+_img!)
                          ),
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Color(0xffe52b5d),
                            width: 2.50,
                          ),
                          color: Colors.white,
                        ),

                      ),
                      onTap: () => Scaffold.of(context).openDrawer(),
                    );
                  }
              ),
              Container(
                width: 123,
                height: 30,
                child: Image.asset("assets/images/logoFlenci.png"),
              ),
              Expanded(
                child: const Text(
                  "Menu",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Mada",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),

            ],
          ),
          
              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            width: 302,
            height: 35,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Color(0xfffbbcdb),
                width: 1.50,
              ),
              color: Colors.white,
            ),
                child: TextFormField(

                  decoration: const InputDecoration(
                    hintText: "Search an instagram account ...",
                    hintStyle:  TextStyle(
                      color: Color(0xff0d0d0d),
                      fontSize: 13,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w500,
                    ),
                    fillColor: Colors.transparent,
                    border: InputBorder.none,
                    suffixIcon: Icon(
                      Icons.search_outlined,
                      size: 30,
                      color: Color(0xfffbbcdb),
                    ),
                    filled: true,
                  ),
                ),
          ),
        ]),
      ),
      body:  RawScrollbar(
             thumbColor: const Color(0xfff4abbb),
             radius: const Radius.circular(20),
             child: Scrollbar(
               thickness: 0,
               child: ListView.builder(
                 itemCount: _instagrameuseList.length,
                 itemBuilder: (BuildContext context, int index) {
                   return MenuCard(
                       _instagrameuseList[index].username,
                     _instagrameuseList[index].id,

                   );
                 },
               ),
             ),
           ));
          }else{
            return const Center(
            child: CircularProgressIndicator(),
            );
            }
          }


      );

  }
}
