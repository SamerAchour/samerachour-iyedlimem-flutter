import 'package:Flenci/models/instagrameuse_class.dart';
import 'package:Flenci/models/offre_class.dart';
import 'package:Flenci/widgets/bord_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http ;
import 'dart:convert';




class Board extends StatefulWidget {
  const Board({Key? key}) : super(key: key);

  @override
  State<Board> createState() => _BoardState();
}

class _BoardState extends State<Board> {
  final List<Offre> _offerList=[];
  late String? _userId;
  bool _pend = false;
  bool _going = true;
  bool _done= false;

  final String _baseUrl = "flenci.herokuapp.com";
  late Future<bool> fetchedInfluencers ;
  Future<bool> getinflu() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _userId = prefs.getString("userId");

    http.Response response = await http.get(Uri.http(_baseUrl, "/api/insta/one/"+_userId!));
    dynamic influfromServer = json.decode(response.body);
    List<dynamic> requests = influfromServer["requests"];
    print(influfromServer);
    for(int i=0 ; i < requests.length ;i++){
      _offerList.add(Offre(requests[i]["company"]["img"], requests[i]["company"]["name"], requests[i]["company"]["field"], requests[i]["budget"]));
    }


    return true;
  }


  @override
  void initState() {
    fetchedInfluencers=getinflu();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(

          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,

            children: [
              SizedBox (height: 50),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/userCompany.png")
                        ),
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: Color(0xffe52b5d),
                          width: 2.50,
                        ),
                        color: Colors.white,
                      ),

                    ),
                    SizedBox(width: 10),
                    const Text('Semer Achour',style: TextStyle(fontSize: 20),),
                  ],
                ),
                onTap: () {
                  Navigator.pushNamed(context, "/");
                },
              ),
              ListTile(
                  title:

                  Divider(height: 000,color: Colors.black,thickness: 2,)


              ),
              ListTile(
                title: Row(
                  children: [
                    const Text('Profil Insights'),
                    SizedBox ( width: 6,),
                    Icon(Icons.insights),
                  ],
                ),
                onTap: () {
                  Navigator.pushNamed(context, "/owner/instaprofile");
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    const Text('Edit Profil'),
                    SizedBox ( width: 6,),
                    Icon(Icons.edit),
                  ],
                ),
                onTap: () {
                  Navigator.pushNamed(context, "/owner/edit");
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    const Text('Sign Out'),
                    SizedBox ( width: 6,),
                    Icon(Icons.logout),
                  ],
                ),
                onTap: () async{
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                  prefs.clear();
                  Navigator.pushNamed(context, "/signin");
                },
              ),

              ListTile(
                title: Row(
                  children: [
                    const Text('Delete Account'),
                    SizedBox ( width: 6,),
                    Icon(Icons.delete),
                  ],
                ),
                onTap: () {},
              ),
            ],
          )),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[Container()],
        toolbarHeight: 100,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        title:
        Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Builder(
                  builder: (context) {
                    return InkWell(
                      child: Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/userCompany.png")
                          ),
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: Color(0xffe52b5d),
                            width: 2.50,
                          ),
                          color: Colors.white,
                        ),

                      ),
                      onTap: () => Scaffold.of(context).openDrawer(),
                    );
                  }
              ),
              Container(
                width: 123,
                height: 30,
                child: Image.asset("assets/images/logoFlenci.png"),
              ),
              Expanded(
                child: const Text(
                  "Board",
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontFamily: "Mada",
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),

            ],
          ),

          Container(
            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
            width: 302,
            height: 35,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Color(0xfffbbcdb),
                width: 1.50,
              ),
              color: Colors.white,
            ),
            child: TextFormField(

              decoration: const InputDecoration(
                hintText: "Search freind ...",
                hintStyle:  TextStyle(
                  color: Color(0xff0d0d0d),
                  fontSize: 13,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w500,
                ),
                fillColor: Colors.transparent,
                border: InputBorder.none,
                suffixIcon: Icon(
                  Icons.search_outlined,
                  size: 30,
                  color: Color(0xfffbbcdb),
                ),
                filled: true,
              ),
            ),
          ),
        ]),
      ),
      body: FutureBuilder(
        future: fetchedInfluencers,
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.hasData){
           return Column(
              children: [

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      child: _pend?
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0xffe12c4c), Color(0xffba3985), Color(0xffcf3275)], ),
                        ),
                        child: Center(
                          child: Text(
                            "Pending",
                            textAlign: TextAlign.center,
                            style:  TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ):
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Color(0xff363636), width: 1, ),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Text(
                            "Pending",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff363636),
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      onTap: ()async{
                        setState(() {
                          _pend=!_pend;
                          if(_pend == true){
                            _going=false;
                            _done=false;
                          }
                        });
                      },
                    ),
                    InkWell(
                      child: _going?
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0xffe12c4c), Color(0xffba3985), Color(0xffcf3275)], ),
                        ),
                        child: Center(
                          child: Text(
                            "Going",
                            textAlign: TextAlign.center,
                            style:  TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ):
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Color(0xff363636), width: 1, ),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Text(
                            "Going",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff363636),
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      onTap: ()async{
                        setState(() {
                          _going=!_going;
                          if( _going== true){
                            _pend=false;
                            _done=false;
                          }
                        });
                      },
                    ),
                    InkWell(
                      child: _done?
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0xffe12c4c), Color(0xffba3985), Color(0xffcf3275)], ),
                        ),
                        child: Center(
                          child: Text(
                            "Done",
                            textAlign: TextAlign.center,
                            style:  TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ):
                      Container(
                        width: 75,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: Color(0xff363636), width: 1, ),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Text(
                            "Done",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xff363636),
                              fontSize: 16,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      onTap: ()async{
                        setState(() {
                          _done=!_done;
                          if( _done== true){
                            _pend=false;
                            _going=false;
                          }
                        });
                      },
                    ),


                    /* Container(
                margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                width: 100,
                height: 40,


                //width: double.infinity,

                child: ElevatedButton(
                  onPressed: () {},

                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(

                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                            side: BorderSide(color: Colors.black)),

                    ),
                  ),
                  child: Container(
                    constraints: const BoxConstraints(
                        minWidth: double.infinity, minHeight: 50.0),
                    alignment: Alignment.center,
                    child: const Text(
                      "Pending",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Mada',
                          fontSize: 18,
                          color: Colors.black),
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                width: 120,
                height: 40,

                //width: double.infinity,

                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0),
                      )),
                      elevation: MaterialStateProperty.all(0.0),
                      backgroundColor: MaterialStateProperty.all(Colors.transparent)),
                  child: Ink(
                    decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          colors: [
                            Color(0xffEB2834),
                            Color(0xffCA337E),
                            Color(0xffA93D8C)
                          ],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                        borderRadius: BorderRadius.circular(15.0)),
                    child: Container(
                      // ink width to fit-parent
                      constraints: const BoxConstraints(
                          minWidth: double.infinity, minHeight: 50.0),
                      alignment: Alignment.center,
                      child: const Text(
                        "Going",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: 'Mada',
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                width: 100,
                height: 40,

                //width: double.infinity,

                child: ElevatedButton(
                  onPressed: () {},

                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(

                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          side: BorderSide(color: Colors.black)),

                    ),
                  ),
                  child: Container(
                    constraints: const BoxConstraints(
                        minWidth: double.infinity, minHeight: 50.0),
                    alignment: Alignment.center,
                    child: const Text(
                      "Done",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Mada',
                          fontSize: 18,
                          color: Colors.black),
                    ),
                  ),
                ),
              ),*/
                  ],
                ),
                Center(
                  child: Container(
                    height: 530,
                    child: RawScrollbar(
                      thumbColor: const Color(0xfff4abbb),
                      radius: const Radius.circular(20),
                      child: Scrollbar(
                        thickness: 0,
                        child: ListView.builder(
                          itemCount: _offerList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return BoardCard(
                              _offerList[index].image,
                              _offerList[index].name,
                              _offerList[index].job,
                              _offerList[index].budget,

                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            );
          }else{
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      )
    );
  }
}
