import 'package:Flenci/screens/signin.dart';
import 'package:Flenci/widgets/nav_bot/nav_bot_insta.dart';
import 'package:Flenci/widgets/nav_bot/nav_bot_owner.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SplachScreen extends StatefulWidget {
  const SplachScreen({Key? key}) : super(key: key);

  @override
  _SplachScreenState createState() => _SplachScreenState();
}

class _SplachScreenState extends State<SplachScreen> {
  late String _route;
  late Future<bool> _session;
  Future<bool> _getSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey("userId")) {
      if(prefs.containsKey("role")){
        _route = "/owner/navbot";
      }else{
        _route = "/insta/navbot";
      }

    } else {
      _route = "/signin";
    }

    return true;
  }
  @override
  void initState() {
    _session = _getSession();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  FutureBuilder(
        future: _session,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData) {
            if (_route == "/signin") {
              return Signin();
            } else if ( _route == "/insta/navbot") {
              return NavBotInsta();
            }else{
              return NavBotOwner();
            }
          } else {
            return Scaffold(
              appBar: AppBar(
                title: const Text("Chargement"),
              ),
              body: const Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }
}
