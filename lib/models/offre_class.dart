class Offre{
  final String image;
  final String name;
  final String job;
  final String budget;

  Offre(this.image, this.name, this.job, this.budget);

  @override
  String toString() {
    return 'Offre{image: $image, name: $name, job: $job, budget: $budget}';
  }
}