class Instagrameuse{
  final String image;
  final String name;
  final String username;
  final String intersets;
  final int followers;
  final String engagment;
  final String impression;
  final int posts;
  final String comments;

  Instagrameuse(
      this.image,
      this.name,
      this.username,
      this.intersets,
      this.followers,
      this.engagment,
      this.impression,
      this.posts,
      this.comments);

  @override
  String toString() {
    return 'Instagrameuse{image: $image, name: $name, username: $username, intersets: $intersets, followers: $followers, engagment: $engagment, impression: $impression, posts: $posts, weeklyposts: $comments}';
  }
}


class Influ{
  final String username;
  final String id;

  Influ(this.username, this.id);

  @override
  String toString() {
    return 'Influ{username: $username, id: $id}';
  }
}